"use strict"

import React from 'react';
import ReactDOM from 'react-dom';
import BasicComponent from '../components/BasicComponent';


function LoadComponent() {
    ReactDOM.render(<BasicComponent />, document.getElementById('Container'));
}
//Load Basic Component
LoadComponent();
