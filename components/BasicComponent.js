"use strict"
import React from 'react';
var Radium = require('radium');

module.exports = Radium(React.createClass({

    handleClick: function(e) {
        e.preventDefault();
        console.log("click");
    },
    handleChange: function(e){
        console.log(e.target.value);
    },
    render: function() {
        return ( 
            <form>
                <input style={[styles.exampleInput, styles.text]} onChange={this.handleChange}></input>
                <button onClick={this.handleClick}>Submit</button>
            </form>
        );
    }
}));
var styles = {
    exampleInput: {
        backgroundColor: "#fff",
        width: "auto",
        padding: "10px",
        position: "relative",
        borderBottom: "1px solid #ebebeb",
        bordertop: "1px solid #fff",
        ":hover": {
            backgroundColor: "#e4e4e4"
        },
        text:{
            color:"#666",
            fontFamily:"arial,helvetica,sans-serif"
        }
    },

}